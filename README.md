# Entity translations helper

## Description

Helper module to expose related translations entities form (add or edit), by
using a modal, into main entity form of non translatable entity reference fields
(with translatable entities only) & when using "Hide non translatable fields on
translation forms" into main entity.

## Installation
Just enable module.
