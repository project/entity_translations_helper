<?php

namespace Drupal\entity_translations_helper;

use Drupal\Core\EventSubscriber\MainContentViewSubscriber;

/**
 * Allows detecting form is rendered into an AJAX page.
 */
trait AjaxDetectionTrait
{

  /**
   * Check we are into an AJAX request.
   *
   * @return bool
   *   TRUE when the _wrapper_format query parameter is one of te drupal ajax forms.
   */
  protected function isAjaxRequest() {
    return in_array($this->requestStack->getCurrentRequest()->get(MainContentViewSubscriber::WRAPPER_FORMAT), [
      'drupal_ajax',
      'drupal_modal',
      'drupal_dialog',
      'drupal_dialog.off_canvas',
    ]);
  }

}
