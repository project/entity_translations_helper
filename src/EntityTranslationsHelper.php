<?php

namespace Drupal\entity_translations_helper;

use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\content_translation\ContentTranslationManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\EventSubscriber\MainContentViewSubscriber;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\BeforeCommand;
use Drupal\Core\Entity\ContentEntityFormInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Link;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Render\Element;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Entity translations helper "EntityTranslationsHelper" service object.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class EntityTranslationsHelper {

  const SELECTOR_BASE = 'entity-translations-helper';
  const SELECTOR_MODAL = self::SELECTOR_BASE . '-modal';
  const SELECTOR_WRAPPER = self::SELECTOR_BASE . '-wrapper';
  const SELECTOR_FIELD_BASE = self::SELECTOR_BASE . '-field-';
  const SELECTOR_ENTITY_BASE = self::SELECTOR_BASE . '-entity-';

  use AjaxDetectionTrait;

  /**
   * A request stack object.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The content translation manager.
   *
   * @var \Drupal\content_translation\ContentTranslationManagerInterface
   */
  protected $contentTranslationsManager;

  /**
   * Entity type manager which performs the upcasting in the end.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a "EntityTranslationsHelper" object.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   A request stack object.
   * @param \Drupal\content_translation\ContentTranslationManagerInterface $content_translations_manager
   *   The content translation manager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(RequestStack $request_stack, ContentTranslationManagerInterface $content_translations_manager, EntityTypeManagerInterface $entity_type_manager) {
    $this->requestStack = $request_stack;
    $this->contentTranslationsManager = $content_translations_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Alter main form.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function alterMainForm(array &$form, FormStateInterface $form_state): void {
    $entity = $this->alterMainFormGetValidEntity($form_state);
    if ($entity instanceof ContentEntityInterface) {
      self::alterMainFormEntityBuidlFieldDetails($form, $entity);
      self::alterMainFormBuidlWrapperDetails($form);
    }
  }

  /**
   * Alter modal form.
   *
   * @param array $form
   *   Form.
   */
  public function alterModalForm(array &$form): void {
    if ($this->alterModalFormAjaxCheck()) {
      $form['#process'][] = [static::class, 'alterModalFormProcess'];
      $form['actions']['submit']['#ajax'] = [
        'callback' => [static::class, 'alterModalFormAjaxCallback'],
        'event' => 'click',
        'disable-refocus' => TRUE,
      ];
    }
  }

  /**
   * Alter modal form process.
   *
   * @param array $element
   *   Form element.
   *
   * @return array
   *   Form element processed.
   */
  public static function alterModalFormProcess(array $element): array {
    // Just leave main submit button to avoid any possible redirection.
    foreach (array_diff(Element::children($element['actions']), ['submit']) as $action_key) {
      $element['actions'][$action_key]['#access'] = FALSE;
    }
    return $element;
  }

  /**
   * Alter modal form ajax callback.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Ajax response.
   *
   * @SuppressWarnings(PHPMD.UnusedFormalParameter)
   */
  public static function alterModalFormAjaxCallback(array $form, FormStateInterface $form_state): AjaxResponse {
    $response = new AjaxResponse();
    $show_messages_inside_modal = TRUE;
    // Show any message & refresh link.
    if (!$form_state->hasAnyErrors()) {
      $response->addCommand(new CloseDialogCommand('.' . Html::cleanCssIdentifier(self::SELECTOR_MODAL)));
      $entity = $form_state->getFormObject()->getEntity();
      $link = self::entityBuildLinkForLanguage($entity, $entity->language());
      $link_html = \Drupal::service('renderer')->render($link);
      $response->addCommand(new ReplaceCommand('.' . Html::cleanCssIdentifier(self::SELECTOR_ENTITY_BASE . $entity->id()), $link_html));
      $show_messages_inside_modal = FALSE;
    }
    $response->addCommand(new BeforeCommand($show_messages_inside_modal ? '.' . Html::cleanCssIdentifier(self::SELECTOR_MODAL) . ' form' : 'form', [
      '#type' => 'status_messages',
    ]));
    return $response;
  }

  /**
   * Obtain main form valid entity.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|null
   *   Entity when valid.
   */
  protected function alterMainFormGetValidEntity(FormStateInterface $form_state): ?ContentEntityInterface {
    $valid_entity = NULL;
    if ($form_state->getFormObject() instanceof ContentEntityFormInterface) {
      $entity = $form_state->getFormObject()->getEntity();
      $main_condition = $entity instanceof ContentEntityInterface && !$entity->isNew() && $entity->isTranslatable() && !$entity->isDefaultTranslation();
      if ($main_condition) {
        $settings = $this->contentTranslationsManager->getBundleTranslationSettings($entity->getEntityTypeId(), $entity->bundle());
        $valid_entity = !empty($settings['untranslatable_fields_hide']) ? $entity : $valid_entity;
      }
    }
    return $valid_entity;
  }

  /**
   * Modal form ajax check.
   *
   * @return bool
   *   When should proceed.
   */
  protected function alterModalFormAjaxCheck(): bool {
    $current_request = $this->requestStack->getCurrentRequest();
    if (!$current_request instanceof Request) {
      return FALSE;
    }
    $wrapper_format = $current_request->get(MainContentViewSubscriber::WRAPPER_FORMAT) ?? '';
    $has_ajax_query_param = $current_request->query->get('entity_translations_helper') === 'ajax';
    return $this->isAjaxRequest() && $has_ajax_query_param;
  }

  /**
   * Entity build link for language.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity.
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   Language.
   *
   * @return array
   *   Link ready to render.
   */
  protected static function entityBuildLinkForLanguage(ContentEntityInterface $entity, LanguageInterface $language): array {
    $link = [];
    if ($entity->isTranslatable()) {
      // When the entity is a node we want its form to open in other tab.
      $popup = $entity->getEntityTypeId() !== 'node';
      $link = self::getRenderableArrayLink($entity, $language, $popup);
    }
    return $link;
  }

  /**
   * Get renderable array.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity.
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   Language.
   * @param bool $popup
   *   If is a popup
   *
   * @return array
   *   Renrerable array.
   */
  protected static function getRenderableArrayLink(ContentEntityInterface $entity, LanguageInterface $language, bool $popup = TRUE): array {
    $url_options = [
      'language' => $language,
      'attributes' => ['target' => '_blank'],
    ];
    if ($popup) {
      $url_options['query'] = ['entity_translations_helper' => 'ajax'];
    }

    // @see Related similar code, access add/edit or use translation form?
    if ($entity->hasTranslation($language->getId())) {
      $url = $entity->toUrl('edit-form', $url_options);
      // @code \Drupal\content_translation\Controller\ContentTranslationController::overview. @endcode
      // @code $url = $entity->toUrl('drupal:content-translation-edit', $url_options)->setRouteParameter('language', $language->getId()); @endcode
      $action_label = t('Edit');
      $label = $entity->getTranslation($language->getId())->label();
    }
    else {
      $url = $entity->toUrl('drupal:content-translation-add', $url_options)->setRouteParameter('source', $entity->getUntranslated()->language()->getId())->setRouteParameter('target', $language->getId());
      $action_label = t('Add');
      $label = $entity->label();
    }

    $link = Link::fromTextAndUrl(t(':action ":language" translation of ":label"', [
      ':language' => $language->getName(),
      ':action' => $action_label,
      ':label' => $label,
    ]), $url)->toRenderable();

    if ($popup) {
      $link['#attributes']['class'][] = 'use-ajax';
      $link['#attributes']['class'][] = Html::cleanCssIdentifier(self::SELECTOR_ENTITY_BASE . $entity->id());
      $link['#attributes']['data-dialog-type'] = 'modal';
      $link['#attributes']['data-dialog-options'] = Json::encode([
        'height' => '75%',
        'width' => '75%',
        'autoResize' => TRUE,
        'classes' => ['ui-dialog-content' => Html::cleanCssIdentifier(self::SELECTOR_MODAL)],
      ]);
      $link['#attached']['library'][] = 'core/drupal.dialog.ajax';
    }
    // Preview!? Condider it should also refreshed into ajax if added.
    // @code $preview = \Drupal::entityTypeManager()->getViewBuilder($entity->getEntityTypeId())->view($entity); @endcode
    // @code $link['#prefix'] = strip_tags(\Drupal::service('renderer')->render($preview), ['img']); @endcode

    return $link;
  }

  /**
   * Alter main form build wrapper details.
   *
   * @param array $form
   *   Form.
   */
  protected static function alterMainFormBuidlWrapperDetails(array &$form): void {
    if (!empty($form['entity_translations_helper'])) {
      $form['entity_translations_helper'] += [
        '#type' => 'details',
        '#weight' => -999,
        '#title' => t('Manage related translations'),
        '#attributes' => [
          'class' => [Html::cleanCssIdentifier(self::SELECTOR_WRAPPER),
          ],
        ],
      ];
    }
  }

  /**
   * Alter main form entity build related field details.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity.
   */
  protected static function alterMainFormEntityBuidlFieldDetails(array &$form, ContentEntityInterface $entity): void {
    $referenced_entities_store = ReferencedEntitiesStore::create();
    self::setReferencedEntitiesInStore($referenced_entities_store, $entity);

    if ($referenced_entities_store->ifReferencedEntities() === FALSE) {
      return;
    }

    $entity_language = $entity->language();
    $field_details = [];
    // Get all the entities grouped by entity type and process.
    foreach ($referenced_entities_store->getReferencedEntities() as $group_referenced_entities) {
      $entity_type_data = $group_referenced_entities['data'];
      $entity_type_key = $entity_type_data['key'];
      $referenced_entities = $group_referenced_entities['items'];

      foreach ($referenced_entities as $referenced_entity) {
        $field_details[$entity_type_key][] = [
          '#type' => 'container',
          'link' => self::entityBuildLinkForLanguage($referenced_entity, $entity_language),
        ];
      }

      $field_details[$entity_type_key] += [
        '#type' => 'details',
        '#title' => $entity_type_data['label'],
        '#attributes' => [
          'class' => [
            Html::cleanCssIdentifier(self::SELECTOR_FIELD_BASE . $entity_type_key),
          ],
        ],
      ];
    }
    $form['entity_translations_helper'] = $field_details;
  }

  /**
   * Get the referenced entities and store them in $referenced_entities_store.
   *
   * @param \Drupal\entity_translations_helper\ReferencedEntitiesStore $referenced_entities_store
   *   Store object.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity to process.
   * @param int $level
   *   Depth of the related entity.
   *
   * @return void
   */
  protected static function setReferencedEntitiesInStore(ReferencedEntitiesStore $referenced_entities_store, ContentEntityInterface $entity, int $level = 0): void {
    $level++;
    // prevents infinite recursion.
    if ($level > 5) {
      return;
    }

    foreach ($entity->getFieldDefinitions() as $field_key => $field_definition) {
      // We want only entity references.
      $field_check = in_array($field_definition->getType(), ['entity_reference', 'entity_reference_revisions']);
      $field = self::getEntityField($field_check, $entity, $field_key);

      // Only the entities from not translatable fields are candidate to be stored.
      $is_store_candidate =  !$field_definition->isTranslatable();

      if (!($field instanceof EntityReferenceFieldItemListInterface && !$field->isEmpty())) {
        continue;
      }

      foreach ($field->referencedEntities() as $referenced_entity) {
        if (!($referenced_entity instanceof ContentEntityInterface && $referenced_entity->isTranslatable())) {
          continue;
        }

        if ($is_store_candidate) {
          $referenced_entities_store->setReferencedEntity($referenced_entity);
        }

        // Only paragraphs can go deeper.
        if ($referenced_entity->getEntityTypeId() === 'paragraph') {
          self::setReferencedEntitiesInstore($referenced_entities_store, $referenced_entity, $level);
        }

      }

    }
  }

  /**
   * Get entity field.
   *
   * @param bool $field_check
   *   Field check.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity.
   * @param string $field_key
   *   Field key.
   */
  protected static function getEntityField(bool $field_check, ContentEntityInterface $entity, string $field_key) {
    return $field_check ? $entity->get($field_key) : NULL;
  }

}
