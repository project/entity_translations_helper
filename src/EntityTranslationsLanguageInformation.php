<?php

namespace Drupal\entity_translations_helper;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\ContentEntityFormInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Link;
use Drupal\language\Entity\ContentLanguageSettings;
use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Allow users knowing the language they are creating the content.
 */
class EntityTranslationsLanguageInformation {

  use AjaxDetectionTrait;

  /**
   * Constructs the information service.
   *
   * @param EntityTypeManagerInterface $entityTypeManager
   *   Used to load entity bundle.
   * @param RequestStack $requestStack
   *   Used to detect page is AJAX.
   * @param RouteMatchInterface $routeMatch
   * @param LanguageManagerInterface $languageManager
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected RequestStack $requestStack,
    protected RouteMatchInterface $routeMatch,
    protected LanguageManagerInterface $languageManager,
    protected RendererInterface $renderer) {
  }

  /**
   * Check form is valid to add the message.
   *
   * @param FormStateInterface $form_state
   *   Form state.
   *
   * @return bool
   *    TRUE when following conditions are met:
   *     - Entity is present and is new.
   *     - Entity is either node, taxonomy_term, media.
   *     - Language is not alterable through a form element.
   *     - Entity has a bundle entity type.
   *     - No ajax call is involved to create the form.
   */
  public function isTranslatableContentCreationForm(FormStateInterface $form_state) {
    $form_object = $form_state->getFormObject();
    if (!$form_object instanceof ContentEntityFormInterface) {
      return FALSE;
    }

    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $form_object->getEntity();
    $language_settings = ContentLanguageSettings::loadByEntityTypeBundle($entity->getEntityTypeId(), $entity->bundle());


    return $entity instanceof ContentEntityInterface && $entity->isNew() && !$this->isAjaxRequest()
      && !$language_settings->isLanguageAlterable()
      && $entity->isTranslatable()
      && in_array($entity->getEntityTypeId(), ['node', 'media', 'taxonomy_term']) && !empty($entity->getEntityType()->getBundleEntityType())
      && $this->routeMatch->getParameter($entity->getEntityType()->getBundleEntityType()) instanceof ConfigEntityBundleBase;
  }

  /**
   * Adds the helper to the form.
   *
   * @param array $form
   *   Form to add the helper.
   * @param FormStateInterface $form_state
   *   Form state to get the current entity.
   */
  public function addLanguageHelper(array &$form, FormStateInterface $form_state) {
    /** @var ContentEntityFormInterface $form_object */
    $form_object = $form_state->getFormObject();
    /** @var ContentEntityInterface $entity */
    $entity = $form_object->getEntity();
    $entity_language = $entity->language();

    /** @var string $bundle_entity_type */
    $bundle_entity_type = $entity->getEntityType()->getBundleEntityType();
    /** @var \Drupal\Core\Config\Entity\ConfigEntityBundleBase $entity_bundle */
    $entity_bundle = $this->entityTypeManager->getStorage($bundle_entity_type)->load($entity->bundle());

    $languages = $this->languageManager->getLanguages();
    // Only show the tip when there is more than one language.
    if (count($languages) > 1) {
      $other_languages = array_filter($languages, function (LanguageInterface $language) use ($entity_language) {
        return $language->getId() != $entity_language->getId();
      });

      /** @var string $route_name */
      $route_name = $this->routeMatch->getRouteName();
      $other_languages_links = array_map(function (LanguageInterface $language) use ($entity_bundle, $bundle_entity_type, $entity, $route_name) {
        return Link::createFromRoute(
          $language->getName(),
          $route_name,
          [$bundle_entity_type => $entity->bundle()],
          ['language' => $language],
        );
      }, $other_languages);
      $form['entity_translations_helper_language'] = [
        '#type' => 'details',
        '#title' => t('You are creating this @bundle in <i>@entity_language</i>. Do you want to create it on a different language?', ['@bundle' => $entity_bundle->label(), '@entity_language' => $entity_language->getName()]),
        'languages' => [
          '#markup' => t('Switch language to: ', ['@bundle' => $entity->bundle()]) . implode(', ', array_map(function ($link) {
              $link_render = $link->toRenderable();
              return $this->renderer->render($link_render);
            }, $other_languages_links)) . '.',
        ],
      ];
    }
  }

}
