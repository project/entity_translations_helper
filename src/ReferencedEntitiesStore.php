<?php

namespace Drupal\entity_translations_helper;

use Drupal\Core\Entity\EntityInterface;

/**
 * Class to storage the related entities of a node.
 */
final class ReferencedEntitiesStore {

  /**
   * Store the entities grouped by type.
   *
   * @var array
   */
  protected $store = [];

  /**
   * Instance a store object.
   *
   * @return static
   */
  public static function create() {
    return new static();
  }

  /**
   * Set a new entity in store.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   */
  public function setReferencedEntity(EntityInterface $entity) {
    if ($this->ifSuitableStoring($entity) === FALSE) {
      return;
    }
    if (!isset($this->store[$entity->getEntityTypeId()])) {
      $this->setEntityTypeInStore($entity);
    }
    if (!isset($this->store[$entity->getEntityTypeId()]['items'][$entity->id()])) {
      $this->store[$entity->getEntityTypeId()]['items'][$entity->id()] = $entity;
    }
  }

  /**
   * Check if the entity can be store.
   *
   * We only want entities with an edit-form link.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return bool
   */
  protected function ifSuitableStoring(EntityInterface $entity): bool {
    static $store = [];
    if (!isset($store[$entity->getEntityTypeId()])) {
      /** @var \Drupal\Core\Entity\EntityTypeInterface $entityType */
      $entityType = $entity->getEntityType();
      $store[$entity->getEntityTypeId()] = $entityType->hasLinkTemplate('edit-form');
    }
    return $store[$entity->getEntityTypeId()];
  }

  /**
   * Set the entity type data in the store.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   */
  protected function setEntityTypeInStore(EntityInterface $entity) {
    /** @var \Drupal\Core\Entity\EntityTypeInterface $entityType */
    $entityType = $entity->getEntityType();
    $this->store[$entity->getEntityTypeId()] = [
      'items' => [],
      'data' => [
        'key' => $entityType->id(),
        'label' => $entityType->getLabel(),
      ],
    ];
  }

  /**
   * Get data from store.
   *
   * @return array
   */
  public function getReferencedEntities(): array {
    return $this->store;
  }

  /**
   * Check if there are stored entities.
   *
   * @return bool
   */
  public function ifReferencedEntities() {
    return count($this->store) > 0;
  }

}
